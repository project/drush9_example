Drush 9 Commands Example

INTRODUCTION

Drush 9 Commands Example project (module) about
"How To Write Custom Drush 9 Commands For Drupal 8 / 9"?

REQUIREMENTS

The project (module) requires Drupal core ^8.4 || ^9 version.

CONFIGURATION

Drush 9.x is the only supported version of Drush in Drupal 8.4.x
and later versions, so it is imperative to learn
how to write custom Drush commands for Drush 9.

This module shows how to write custom Drush 9 commands for Drupal 8/9.

INSTALLATION

Install as independent Drupal module installation.
